import child_process  from 'child_process';
import fs             from 'fs';
import yaml           from 'js-yaml';

function logErrors(error, deploy) {
  const stdout = error.stdout.toString()
  const stderr = error.stderr.toString()
  console.log(stdout);
  console.log(stderr);
  if (deploy.yaml.logs === true) {
    const today = new Date().toISOString().replace('Z', '').split('T');
    if (!fs.existsSync(`../.sfdx/logs`)) {
      fs.mkdirSync(`../.sfdx/logs`);
      if (!fs.existsSync(`../.sfdx/logs/${today[0]}`)) {
        fs.mkdirSync(`../.sfdx/logs/${today[0]}`);
      }
    }
    fs.appendFileSync(`../.sfdx/logs/${today[0]}/${today[1].split('.')[0].replace(/:/g,'_')}.txt`, `${stdout}\n${stderr}`, { encoding: 'utf-8' });
  }
}

(() => {
  const deploy = {
    yaml: yaml.load(fs.readFileSync('../config/deployment.yaml', { encoding: 'utf-8' })),
    args: process.argv.slice(2),
  };
  if (deploy.args.length > 0) {
    try {
      child_process.execSync(`sfdx force:source:push -u ${deploy.args[0]}${deploy.yaml.force ? ' -f' : ''}`, { stdio: 'pipe' });
    } catch(error) {
      if (error.stderr.toString().includes('You have sent us an Illegal URL or an improperly formatted request.')) {
        console.log('source:push failed, Starting source:deploy, because of Error Illegal URL');
        if (deploy.yaml.deployAll === false){
          if (deploy.yaml.manifest === null){
            console.log('Deploy all is turned off And manifest is empty');
          } else {
            try {
              const sourceDeploy = child_process.execSync(`cd .. && sfdx force:source:deploy -u ${deploy.args[0]} -p "${deploy.yaml.manifest.join(',')}"`, { stdio: 'pipe' });
              console.log(sourceDeploy.toString());
            } catch(error) {
              logErrors(error, deploy);
            }
          }
        }
        if (deploy.yaml.deployAll === true){
          try {
            const sourceDeploy = child_process.execSync(`cd .. && sfdx force:source:deploy -u ${deploy.args[0]} -p src`, { stdio: 'pipe' });
            console.log(sourceDeploy.toString());
          } catch(error) {
            logErrors(error, deploy);
          }
        }
      } else {
        logErrors(error, deploy);
      }
    }
  } else {
    console.log(`You Didn't specify user: [npm run deploy (scratch org alias)]`)
  }
})();
