import axios from 'axios';
import child_process from 'child_process';

const ExitCodeEnum = Object.freeze({
  0: "Pass",
  1: "Error",
  2: "Unknown"
});

const StatusCodeColorsEnum = Object.freeze({
  "Pass": "#339900",
  "Error": "#cc3300",
  "Unknown": "#ffcc00"
});

const BranchPipelineType = Object.freeze({
  "feature": "Validate",
  "bugfix": "Validate",
  "dxdev": "Deployment",
  "heads/release": "Deployment",
  "main": "Deployment"
});

(() => {
  let bitbucket = {
    branch: child_process.execSync('cd .. && git name-rev --name-only HEAD', { encoding: 'utf-8' }).replace('\n', ''),
    author: {
      name:   child_process.execSync(`cd .. && git log -1  --pretty=format:'%an'`, { encoding: 'utf-8' }),
      email:  child_process.execSync(`cd .. && git log -1  --pretty=format:'%ae'`, { encoding: 'utf-8' })
    },
    description:  child_process.execSync(`cd .. && git log -1 --pretty=%B`, { encoding: 'utf-8' }).replace('\n', ''),
    exitCode:     parseInt(child_process.execSync('cd .. && echo $BITBUCKET_EXIT_CODE', { encoding: 'utf-8' })),
    origin:       child_process.execSync(`cd .. && echo $BITBUCKET_GIT_HTTP_ORIGIN`, { encoding: 'utf-8' }),
    buildNumber:  child_process.execSync(`cd .. && echo $BITBUCKET_BUILD_NUMBER`, { encoding: 'utf-8' }),
  }
  if (bitbucket.branch.includes('feature') || bitbucket.branch.includes('bugfix')) {
    bitbucket.branchType = bitbucket.branch.split('/')[0];
  } else {
    bitbucket.branchType = bitbucket.branch;
  }
  axios.post('https://chat.googleapis.com/v1/spaces/AAAAFS-EzsU/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=57yNFojJcmM9bKdJ1CZBPU-uO2ZQLITrxxwyvpyiJ9s%3D', {
    'cards': [
      {
        'header': {
          'title': 'Bitbucket Pipelines',
          'subtitle': `Project: AwesomeTime`,
          'imageUrl': 'https://bytebucket.org/ravatar/%7B6640e3e4-ac6b-4bf6-9a08-74f1cf749388%7D?ts=2161736',
          'imageStyle': 'AVATAR',
        },
        'sections': [
          {
            'widgets': [
              {
                "keyValue": {
                  'content': `<a href="${bitbucket.origin + '/addon/pipelines/home#!/results/' + bitbucket.buildNumber}">Pipeline</a>`
                }
              },
            ]
          },
          {
            'widgets': [
              {
                "keyValue": {
                  'topLabel': "Type",
                  'content': `${BranchPipelineType[bitbucket.branchType]}`
                }
              },
              {
                "keyValue": {
                  'topLabel': "Status",
                  'content': `<font color="${StatusCodeColorsEnum[ExitCodeEnum[bitbucket.exitCode]]}"><b>${ExitCodeEnum[bitbucket.exitCode]}</b></font>`
                }
              },
              {
                "keyValue": {
                  'topLabel': "Branch",
                  'content': `${bitbucket.branch}`
                }
              },
              {
                "keyValue": {
                  'topLabel': "Author",
                  'content': `${bitbucket.author.name} <p>[${bitbucket.author.email}]</p>`
                }
              },
              {
                "keyValue": {
                  'topLabel': "Description",
                  'content': `${bitbucket.description}`
                }
              }
            ]
          }
        ]
      }
    ]  
  }).catch(function(error) {
    console.error(error);
  })
})();
